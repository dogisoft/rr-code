//<![CDATA[
$(function() { $(document).pngFix(); });

// Initialize the Ushahidi namespace
Ushahidi.baseUrl = "http://romareact.org/";
Ushahidi.markerRadius = 4;
Ushahidi.markerOpacity = 0.8;
Ushahidi.markerStokeWidth = 2;
Ushahidi.markerStrokeOpacity = 0.3;

// Default to most active month
var startTime = 1330556400;

// Default to most active month
var endTime = 1446332399;

// To hold the Ushahidi.Map reference
var map = null;


/**
 * Toggle Layer Switchers
 */
function toggleLayer(link, layer) {
    if ($("#"+link).text() == "Hide")
    {
        $("#"+link).text("Show");
    }
    else
    {
        $("#"+link).text("Hide");
    }
    $('#'+layer).toggle(500);
}

/**
 * Create a function that calculates the smart columns
 */
function smartColumns() {
    //Reset column size to a 100% once view port has been adjusted
    $("ul.content-column").css({ 'width' : "100%"});

    //Get the width of row
    var colWrap = $("ul.content-column").width();

    // Find how many columns of 200px can fit per row / then round it down to a whole number
    var colNum = 1;

    // Get the width of the row and divide it by the number of columns it
    // can fit / then round it down to a whole number. This value will be
    // the exact width of the re-adjusted column
    var colFixed = Math.floor(colWrap / colNum);

    // Set exact width of row in pixels instead of using % - Prevents
    // cross-browser bugs that appear in certain view port resolutions.
    $("ul.content-column").css({ 'width' : colWrap});

    // Set exact width of the re-adjusted column
    $("ul.content-column li").css({ 'width' : colFixed});
}

/**
 * Callback function for rendering the timeline
 */
function refreshTimeline() {

}


jQuery(function() {
    var reportsURL = "json/cluster";

    // Render thee JavaScript for the base layers so that
    // they are accessible by Ushahidi.js
    var bing_road = new OpenLayers.Layer.Bing({
        name: "Bing-Road",
        type: "Road",
        key: "Akrgfvtlbxzp8YbqLteD5oXFWtbYR-t64Hn25FPgf45Ise5PnIjFHF2OT-L9cwPs"
    });

    var bing_hybrid = new OpenLayers.Layer.Bing({
        name: "Bing-Hybrid",
        type: "AerialWithLabels",
        key: "Akrgfvtlbxzp8YbqLteD5oXFWtbYR-t64Hn25FPgf45Ise5PnIjFHF2OT-L9cwPs"
    });

    var bing_satellite = new OpenLayers.Layer.Bing({
        name: "Bing-Satellite",
        type: "Aerial",
        key: "Akrgfvtlbxzp8YbqLteD5oXFWtbYR-t64Hn25FPgf45Ise5PnIjFHF2OT-L9cwPs"
    });


    // Map configuration
    var config = {

        // Zoom level at which to display the map
        zoom: 3,

        // Redraw the layers when the zoom level changes
        redrawOnZoom: true,

        // Center of the map
        center: {
            latitude: 41.153332,
            longitude: 20.168330999999966		},

        // Map controls
        mapControls: [
            new OpenLayers.Control.Navigation({ dragPanOptions: { enableKinetic: true } }),
            new OpenLayers.Control.Attribution(),
            new OpenLayers.Control.Zoom(),
            new OpenLayers.Control.MousePosition({
                div: document.getElementById('mapMousePosition'),
                numdigits: 5
            }),
            new OpenLayers.Control.Scale('mapScale'),
            new OpenLayers.Control.ScaleLine(),
            new OpenLayers.Control.LayerSwitcher()
        ],

        // Base layers
        baseLayers: [bing_road,bing_hybrid,bing_satellite],

        // Display the map projection
        showProjection: true

    };

    // Initialize the map
    map = new Ushahidi.Map('map', config);
    map.addLayer(Ushahidi.GEOJSON, {
        name: "Reports",
        url: reportsURL,
        transform: false
    }, true, true);


    // Register the referesh timeline function as a callback
    map.register("filterschanged", refreshTimeline);
    setTimeout(function() { refreshTimeline(); }, 1500);


    // Category Switch Action
    $("ul#category_switch li > a").click(function(e) {

        var categoryId = this.id.substring(4);
        var catSet = 'cat_' + this.id.substring(4);

        // Remove All active
        $("a[id^='cat_']").removeClass("active");

        // Hide All Children DIV
        $("[id^='child_']").hide();

        // Add Highlight
        $("#cat_" + categoryId).addClass("active");

        // Show children DIV
        $("#child_" + categoryId).show();
        $(this).parents("div").show();

        // Update report filters
        map.updateReportFilters({c: categoryId});

        e.stopPropagation();
        return false;
    });

    // Layer selection
    $("ul#kml_switch li > a").click(function(e) {
        // Get the layer id
        var layerId = this.id.substring(6);

        var isCurrentLayer = false;
        var context = this;

        // Remove all actively selected layers
        $("#kml_switch a").each(function(i) {
            if ($(this).hasClass("active")) {
                if (this.id == context.id) {
                    isCurrentLayer = true;
                }
                map.trigger("deletelayer", $(".layer-name", this).html());
                $(this).removeClass("active");
            }
        });

        // Was a different layer selected?
        if (!isCurrentLayer) {
            // Set the currently selected layer as the active one
            $(this).addClass("active");
            map.addLayer(Ushahidi.KML, {
                name: $(".layer-name", this).html(),
                url: "json/layer/" + layerId
            });
        }

        return false;
    });

    // Timeslider and date change actions
    $("select#startDate, select#endDate").selectToUISlider({
        labels: 4,
        labelSrc: 'text',
        sliderOptions: {
            change: function(e, ui) {
                var from = $("#startDate").val();
                var to = $("#endDate").val();

                if (to > from && (from != startTime || to != endTime)) {
                    // Update the report filters
                    startTime = from;
                    endTime = to;
                    map.updateReportFilters({s: from, e: to});
                }

                e.stopPropagation();
            }
        }
    });

    // Media Filter Action
    $('.filters li a').click(function() {
        var mediaType = parseFloat(this.id.replace('media_', '')) || 0;

        $('.filters li a').attr('class', '');
        $(this).addClass('active');

        // Update the report filters
        map.updateReportFilters({m: mediaType});

        return false;
    });

    //Execute the function when page loads
    smartColumns();

    // Are checkins enabled?

});

$(window).resize(function () {
    //Each time the viewport is adjusted/resized, execute the function
    smartColumns();
});
//]]>/**
