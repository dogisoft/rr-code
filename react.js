jQuery(document).ready(function( $ ) {

    // TOGGLE DROPDOWN
    $('.header_nav_dropdown .header_nav_cancel').on('click', function(e) {
        $(this).closest('.header_nav_dropdown').fadeOut('fast');
        $(this).closest('.header_nav_dropdown').siblings('p').removeClass('active');
    });
    $('.header_nav_has_dropdown > a, .header_nav_actions .header_nav_button_delete, .header_nav_actions .header_nav_button_change').on('click', function(e) {
        $(this).toggleClass('active');
        $(this).siblings('.header_nav_dropdown').fadeToggle('fast')
        e.stopPropagation();
        return false;
    });
    $('.header_nav_actions .header_nav_dropdown').on('click', function(e) {
        e.stopPropagation();
    });

    $('#header_nav_forgot').click(function() {
        $('#header_nav_userforgot_form').toggle('fast');
    });

    // Silence JS errors if console not defined (ie. not firebug and not running chrome)
    if(typeof(console) === 'undefined') {
        var console = {};
        console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = function() {};
    }

});


function toTop()
{
    $('#romareact-up').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });
}

function forselect(id)
{
    if($('#myselect-block'+id).is(":hidden"))
    {
        document.getElementById('foliee').style.display="block";
        $('#myselect'+id).addClass("nyitott"+id);
        $('#myselect-block'+id).slideDown('slow', function() {
            $('#myselect-block'+id).jScrollPane();
        });
    }
    else
    {
        $('.myselect-block').slideUp('fast', function() {
            document.getElementById('foliee').style.display="none";
        });
    }
}

function closeSelects()
{
    $('.myselect-block').slideUp('fast', function() {
        document.getElementById('foliee').style.display="none";
    });
}

function setSelected(id, blokkid)
{
    cont = document.getElementById(id).innerHTML;
    document.getElementById('myselect_data_'+blokkid).innerHTML=cont;
    document.getElementById('incident_date').value=cont;
    document.getElementById('incident_hour').value='12';
    document.getElementById('incident_minute').value='00';
    closeSelects();
}

function addUpload(szam)
{
    cont = document.getElementById('addimg').innerHTML;
    document.getElementById('addimg').innerHTML=cont+"<div><input id=\'news_image\' type=\'file\' name=\'news_image"+szam+"[]\' /></div>";
}

function slider(id)
{
    aktiv_volt = document.getElementById('miaktiv').value;
    document.getElementById('miaktiv').value=id;
    $('#kep'+aktiv_volt).fadeOut("fast", function(){
        $('#kep'+id).fadeIn("fast", function(){
        });
    });
    $('#menu'+aktiv_volt).removeClass("active");
    $('#menu'+id).addClass("active");
    $('#content'+id).slideDown('slow', 'linear', function() { });
    $('#content'+aktiv_volt).slideUp('slow', 'linear', function() 	{ });
}

function sendPetition()
{
    fname = document.getElementById('peti_first').value;
    lname = document.getElementById('peti_last').value;
    mail = document.getElementById('peti_mail').value;
    country = document.getElementById('peti_country').value;

    $.ajax({
        type: "POST",
        url: "/petition.php",
        data: "fname="+fname+"&lname="+lname+"&mail="+mail+"&country="+country
    }).done(function( msg ) {
        document.getElementById('petition_message').innerHTML=msg;
    });
}

function runner()
{
    allas = document.getElementById('allas').value;
    if(allas==100)
    {
        //itt nincs semmi se
    }
    else
    {
        slider(allas);
        if(allas==3)
        {
            allas = -1;
        }
        console.log('me kamav javascriptura');
        document.getElementById('allas').value=parseInt(allas)+parseInt(1);
    }
}

function setHundred()
{
    document.getElementById('allas').value=100;
}

function sendSubscribe()
{
    adat = document.getElementById('subscribe').value;
    if(adat)
    {
        if(IsValidEmail(adat))
        {
            insertSubscribe(adat);
        }
        else
        {
            $( "#dialog-mailerror" ).dialog();
        }
    }
}

function IsValidEmail(email)
{
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return filter.test(email);
}


function insertSubscribe(adat)
{
    $.ajax({
        type: "POST",
        url: "/subscribe.php",
        data: "fname=fname&mail="+adat
    }).done(function( msg ) {
        document.getElementById('dialog').innerHTML=msg;
        $( "#dialog" ).dialog();
    });

}

function sendForm()
{
    rctitle = document.getElementById('rctitle').value;
    rcdesc = document.getElementById('desc').value;
    if(rctitle!="" || rctitle!="Title goes here")
    {
        alert('send')
    }
}

function pollsDisplay()
{
    polid = $("#pollid").val();
    $.ajax({
        type: "POST",
        url: "/chkpoll.php",
        data: "polid="+polid+"&mvote=mvote"
    }).done(function( msg ) {
        $("#voteresults").html(msg);
    });

    $("#polls").hide();
    $("#pollsDisplay").show();
}

function pollsClose()
{
    $("#pollsDisplay").hide();
    $("#polls").show();
}

function vote(polid)
{
    myvote = $("#polform input[type='radio']:checked").val();
    if(myvote!=undefined)
    {
        $.ajax({
            type: "POST",
            url: "/vote.php",
            data: "polid="+polid+"&mvote="+myvote
        }).done(function( msg ) {
            $("#voteresults").show();
            $("#voteresults").html(msg);
        });
    }
}

function disableForm()
{
    $('input[type=radio]').attr('disabled','true');
    $("#voteButt").hide();
}

function setVotedMsg(id)
{
    $('#radio'+id).attr('checked', true);

    $("#voteresults").show();
    message = $("#szazalekok").html();
    $("#voteresults").html(message);
}

function setHideRes()
{
    $("#voteresults").hide();
}